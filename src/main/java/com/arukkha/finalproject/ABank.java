/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.arukkha.finalproject;

/**
 *
 * @author Coke
 */
public class ABank {
    private String name;
    private int balance;
    private String bank;
    private String description;
    private int moneyinout;

    public int getMoneyinout() {
        return moneyinout;
    }

    public void setMoneyinout(int moneyinout) {
        this.moneyinout = moneyinout;
    }

    public ABank(int moneyinout) {
        this.moneyinout = moneyinout;
    }

    public ABank(String name, int money, String bank, String description) {
        this.name = name;
        this.balance = money;
        this.bank = bank;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int money) {
        this.balance = money;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Bank{" + "name=" + name + ", money=" + balance + ", bank=" + bank + ", description=" + description + '}';
    }
    
}
